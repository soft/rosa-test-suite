%if "%dist" == ".res6"
  %define distro rosa-server2012
%else
  %define distro rosa
  %define debug_package %{nil}
%endif

%define modules memfreetest pcietest nlkm
Summary: Rosa test suite
Name: rosa-test-suite
Version: 1.1
Release: 7
License: GPL
%if "%distro" == "rosa-server2012"
Group: System Environment/Base
%else
Group: System/Base
%endif
Source0: %{name}-%{version}.tar.xz
BuildRoot: %{_tmppath}/%{name}-buildroot
BuildRequires: autoconf
BuildRequires: gcc
%if "%distro" == "rosa-server2012"
BuildRequires: at-spi-devel
BuildRequires: glib2-devel
BuildRequires: libvncserver-devel
%else
BuildRequires: pkgconfig(directfb)
BuildRequires: pkgconfig(libspi-1.0)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(libvncclient)
%endif
BuildRequires: kernel-headers
BuildRequires: pkgconfig(libselinux)
BuildRequires: pkgconfig(x11)
BuildRequires: gettext
BuildRequires: net-snmp-devel

Requires: dkms
Requires: kernel-devel
Requires: gcc
Requires: amtu
Requires: pamtester
%if "%distro" == "rosa-server2012"
Requires: audit-libs-python
Requires: libselinux-python
Requires: pygtk2

#Python 2.6 standard library does not have argparse module
Requires: python-argparse
%else
Requires: python-audit
Requires: python-selinux
Requires: pygtk2.0
Requires: python-kde4

Requires: libreoffice-gnome
%endif
Requires: pkgconfig(dbus-python)
Requires: socat
Requires: lm_sensors
Requires: selinux-policy-mls

%description
Rosa test suite package is the set of Rosa OS tests.
%prep
%setup -q
msgfmt -o test_data/l10n/ru/LC_MESSAGES/test_l10n.mo test_data/l10n/ru/LC_MESSAGES/test_l10n.po


%build
if [ -f configure.in ]; then
  autoconf
fi
%configure
%{__make} DISTRO=%{distro}

%install
%{__make} install DISTRO=%{distro} DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_sbindir}
ln -f -s %{_datadir}/%{name}/%{name}.py %{buildroot}%{_sbindir}/rosa-test-suite

%pre
# A hack to allow for upgrade from 1.0.14-
pre_rts_version=`rpm -q --qf "%{VERSION}" rosa-test-suite`
if [ "$pre_rts_version" == "1.0" ]; then
# Replace policy of 1.0.14- with an empty one to prevent conflicts
semodule -s mls -r rosatest
echo 'policy_module(rosatest,1.0)' > %{_datadir}/rosa-test-suite/policy/rosaselftest.te
echo '' > %{_datadir}/rosa-test-suite/policy/rosaselftest.if
echo '' > %{_datadir}/rosa-test-suite/policy/rosaselftest.fc
make NAME=mls DISTRO=%{distro} -C %{_datadir}/rosa-test-suite/policy/ -f %{_datadir}/selinux/devel/Makefile
semodule -s mls -i %{_datadir}/rosa-test-suite/policy/rosaselftest.pp
make NAME=mls DISTRO=%{distro} -C %{_datadir}/rosa-test-suite/policy/ -f %{_datadir}/selinux/devel/Makefile clean
restorecon -F -R %{_datadir}/rosa-test-suite/ %{_libexecdir}/rosa-test-suite/
fi

%post
#remove preinstalled version if any (until we take %release into account)
dkms remove -m %{name} -v %{version} --all
dkms add -m %{name} -v %{version} --rpm_safe_upgrade
dkms build -m %{name} -v %{version} --rpm_safe_upgrade
dkms install -m %{name} -v %{version} --rpm_safe_upgrade --force
make NAME=mls DISTRO=%{distro} -C %{_datadir}/%{name}/policy/ -f %{_datadir}/selinux/devel/Makefile
semodule -s mls -i %{_datadir}/%{name}/policy/rosa-test.pp
make NAME=mls DISTRO=%{distro} -C %{_datadir}/%{name}/policy/ -f %{_datadir}/selinux/devel/Makefile clean
restorecon -F -R %{_datadir}/%{name}/ %{_libexecdir}/%{name}/

%clean
rm -rf %{buildroot}

%preun
if [ $1 == 0 ]; then
semodule -s mls -r rosa-test
restorecon -F -R %{_datadir}/%{name}/ %{_libexecdir}/%{name}/ /var/log/rosa-test-suite.*
fi
dkms remove -m %{name} -v %{version} --rpm_safe_upgrade --all
true


%files
%defattr(-,root,root)
%dir %{_libexecdir}/%{name}
%{_libexecdir}/%{name}/semtest
%{_libexecdir}/%{name}/signaltest
%{_libexecdir}/%{name}/shmemtest
%{_libexecdir}/%{name}/netlinktest
%{_libexecdir}/%{name}/testusb
%if "%distro" == "rosa-server2012"

%else
%{_libexecdir}/%{name}/fbtest
%{_libexecdir}/%{name}/ffstest

%{_libexecdir}/%{name}/test_lowriter
%{_libexecdir}/%{name}/test_localc
%{_libexecdir}/%{name}/test_loimpress
%{_libexecdir}/%{name}/test_kolourpaint
%endif
%{_libexecdir}/%{name}/test_app_title
%{_libexecdir}/%{name}/test_app_title_x11
%{_libexecdir}/%{name}/test_vnc
%{_libexecdir}/%{name}/test_snmp
%{_libexecdir}/%{name}/test_forceclose
%{_libexecdir}/%{name}/npttest
%{_libexecdir}/%{name}/netlabel_server
%{_libexecdir}/%{name}/netlabel_client
%{_libexecdir}/%{name}/test_firefox
%{_libexecdir}/%{name}/hcd-tests.sh
%{_libexecdir}/%{name}/rbac-self-test-helper
%{_libexecdir}/%{name}/pamtest.sh
%{_libexecdir}/%{name}/rutokentest.sh
%{_libexecdir}/%{name}/memshredtest.sh
%{_sbindir}/rosa-test-suite

%dir %{_datadir}/%{name}
%{_datadir}/%{name}/config.xml
%{_datadir}/%{name}/*.py*
%dir %{_datadir}/%{name}/modules
%{_datadir}/%{name}/modules/*.py*
%dir %{_datadir}/%{name}/modules/pexpect
%{_datadir}/%{name}/modules/pexpect/*.py*
%dir %{_datadir}/%{name}/modules/pyip
%{_datadir}/%{name}/modules/pyip/*.py*
%dir %{_datadir}/%{name}/policy
%{_datadir}/%{name}/policy/*
%{_datadir}/%{name}/test_data

%dir %{_usrsrc}/%{name}-%{version}
%{_usrsrc}/%{name}-%{version}/*

